This is a Simple virtual Dhcp Client.

* Run Bash Script to execute the Program

* This Program does not make any changes to the original dhcp config files, so its safe to run as Dhcp Release and Ack doesnt effect the Dhclient service of Linux

* This project makes use of Raw socket so only the Root User can run it.

* This project is Os dependent (Works only on linux based Os)

* The Program needs Port 68 to communicate, so free the port before runnning.

* Freeing the port does not make Permenant changes, as The Kernal keep the port running after some time of disabling.
