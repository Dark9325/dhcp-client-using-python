from struct import *
import socket

class raw:
    def __init__(self,data):
        self.source_ip = '0.0.0.0'
        self.dest_ip = '255.255.255.255'
        self.data = data
        self.udp_sport = 68
        self.udp_dport = 67
        self.ip_header()
        self.udp_header()
        
    def ip_header(self):
        ip_ihl = 5
        ip_ver = 4
        ip_tos = 0
        ip_tot_len = 0	# kernel will fill the correct total length
        ip_id = 54321	#Id of this packet
        ip_frag_off = 0
        ip_ttl = 255
        ip_proto = socket.IPPROTO_UDP
        ip_check = 0
        ip_saddr = socket.inet_aton (self.source_ip)	#Spoof the source ip address if you want to
        ip_daddr = socket.inet_aton (self.dest_ip)

        ip_ihl_ver = (ip_ver << 4) + ip_ihl

            # the ! in the pack format string means network order
        ip_header_pack = pack('!BBHHHBBH4s4s' , ip_ihl_ver, ip_tos, ip_tot_len, ip_id, ip_frag_off, ip_ttl, ip_proto, ip_check, ip_saddr, ip_daddr)
        return ip_header_pack;

    def udp_header(self):
        udp_raw_sport = self.udp_sport   # arbitrary source port
        udp_raw_dport = self.udp_dport # arbitrary destination port
        udp_checksum = 0
        udp_header = pack('!HHH', udp_raw_sport, udp_raw_dport,udp_checksum)
        udp_length = 8 + len(self.data)
        udp_check = 0

        udp_header_pack = pack('!HHH', udp_raw_sport, udp_raw_dport, udp_length) + pack('H' , udp_check)
        return udp_header_pack;

    def raw_header(self):
        raw_header_pack = ''
        raw_header_pack = self.ip_header() + self.udp_header() + self.data
        return raw_header_pack
