import socket
import ipaddress
import json
import os
import time
from sys import exit
from Dora import Dhcp_Discover, Dhcp_Offer, Dhcp_Request, Dhcp_Ack, Dhcp_Release
from Raw_Packets import raw

def socket_com():
    try:
        raw_sck = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_RAW)
    except socket.error as e:
        print('Socket could not be created.\n', e)
        exit()

    udp_sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_sck.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    try:
	    udp_sck.bind(('',68))
    except Exception as e:
        print('port 68 in use, closing connection...Try command (fuser -k 68/udp)and then rerun the program\n')
        udp_sck.close()
        exit()

    dhcp_disc_obj = Dhcp_Discover()
    data = dhcp_disc_obj.Dhcp_Discover_packet()

    raw_header_obj = raw(data)
    raw_packet = raw_header_obj.raw_header()

    raw_sck.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    raw_sck.sendto(raw_packet, (raw_header_obj.dest_ip , 0 ))

    print('DHCP Discover sent waiting for reply....\n')
    udp_sck.settimeout(5) #try till 5 secs
    try:
        while True:
            time.sleep(3)
            dhcp_offer_data = udp_sck.recv(1024)
            if dhcp_offer_data:
                raw_sck.close()
                print("DHCP Offer Recieved, Response are as follows:\n")
                dhcp_offer_obj = Dhcp_Offer(dhcp_offer_data, dhcp_disc_obj.get_trans_id)
                dhcp_offer_obj.printOffer()
                print('\n')
                user_choice = raw_input("Accept Offer and send Request? (Y/N)\n")

                if user_choice.upper() == 'Y':
                    dhcp_req_obj = Dhcp_Request(dhcp_offer_data)
                    udp_sck.sendto(dhcp_req_obj.Dhcp_Request_packet(), (str(ipaddress.IPv4Address(dhcp_offer_data[251:255])),67))
                    print("DHCP Request sent waiting for Ack...\n")
                    try:
                        while True:
                            dhcp_ack_data = udp_sck.recv(1024)
                            time.sleep(3)
                            if dhcp_ack_data:
                                print('###############\n')
                                print("Ack Recieved !!!\n")
                                print('###############\n')
                                time.sleep(4)
                                dhcp_ack_obj = Dhcp_Ack(dhcp_ack_data, dhcp_disc_obj.get_trans_id)
                                dhcp_ack_obj.save_Ack()
                                udp_sck.close()
                                break
                    except socket.timeout as e:
                        print("Failed to receive Dhcp Ack, Invalid mac: Media device Disconnected\n")
                        udp_sck.close()
                        exit()
                else:
                    print("Request not sent, Offer Declined.\n")
                    udp_sck.close()
            break

    except socket.timeout as e:
        print("Failed to receive Dhcp Offer\n")
        udp_sck.close()
        raw_sck.close()
    raw_sck.close()

def socket_open_ack(filename):
    try:
        with open(filename) as json_file:
             if os.path.getsize('ack.txt') < 0:
                 print('Ack info does not exist\n')
             else:
                 json_data = json.load(json_file)
                 return json_data
    except (OSError, IOError) as e:
        print("Ack details not found")
        exit()
        
def socket_read():
    data = socket_open_ack('ack.txt')
    print("Ack info are as follow:\n")
    time.sleep(2)
    print("Ip Address:" + str(data["Ip address"]))
    print("Subnet Mask:" + str(data["subnet mask"]))
    print("Dhcp Server:" + str(data["Dhcp Server"]))
    print('')

def socket_release():
     data = socket_open_ack('ack.txt')
     ip_addr = socket.inet_aton(str(data["Ip address"]))
     server_addr = socket.inet_aton(str(data["Dhcp Server"]))
     dhcp_sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     dhcp_sck.settimeout(5)
     try:
         dhcp_sck.bind(('',68))
     except Exception as e:
          print('port 68 in use, closing connection...Try command (fuser -k 68/udp), and then rerun the program\n')
          dhcp_sck.close()
          exit()
     dhcp_request_obj = Dhcp_Release(ip_addr, server_addr)
     dhcp_sck.sendto(dhcp_request_obj.Dhcp_Release_packet(),(data['Ip address'],67))
     print('Dhcp Release Sent\n')
    
     try:
        while True:
            os.remove('ack.txt')
            print('Ip released\n')
            dhcp_sck.close()
            exit()
     except socket.timeout as e:
        print(e)
        dhcp_sck.close()
          
