from Get import get_class
import struct
import ipaddress
import json

class Dhcp_Discover(get_class):
    def __init__(self):
        get_class.__init__(self)

    def Dhcp_Discover_packet(self):
        opcode = b'\x01'
        hardware_type = b'\x01'
        hardware_addr_len = b'\x06'
        hop_count = b'\x00'
        trans_id = self.get_trans_id
        num_of_sec = b'\x00\x00'
        broadcast_flag = b'\x80\x00'
        client_ip = b'\x00\x00\x00\x00'
        allocated_ip = b'\x00\x00\x00\x00'
        server_ip = b'\x00\x00\x00\x00'
        relay_agent_ip = b'\x00\x00\x00\x00'
        client_mac_addr = self.get_mac_in_bytes
        client_addr_padding = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        server_host_name = b'\x00' * 67
        boot_file_name = b'\x00' * 125
        opt_dhcp_magic_cookie = b'\x63\x82\x53\x63'
        opt_dhcp_type = b'\x35\x01\x01'
        opt_mac = b'\x3d\x06' + self.get_mac_in_bytes
        opt_param =  b'\x37\x03\x03\x01\x06'
        opt_end = b'\xff'
        packet = opcode + hardware_type + hardware_addr_len + hop_count + trans_id + num_of_sec + broadcast_flag
        packet += client_ip + allocated_ip + server_ip + relay_agent_ip
        packet += client_mac_addr + client_addr_padding + server_host_name + boot_file_name
        packet += opt_dhcp_magic_cookie + opt_dhcp_type + opt_mac + opt_param + opt_end
        return packet;

class Dhcp_Offer:
    def __init__(self, data, transID):
        self.data = data
        self.transID = transID
        self.offerIP = ''
        self.nextServerIP = ''
        self.DHCPServerIdentifier = ''
        self.leaseTime = ''
        self.router = ''
        self.subnetMask = ''
        self.unpack(self.data)

    def unpack(self,data):
        if data[4:8] == self.transID :
            self.offerIP = ipaddress.IPv4Address(data[16:20])
            self.DHCPServerIdentifier = ipaddress.IPv4Address(data[251:255])#273:277?
            self.leaseTime = struct.unpack('!L',data[268:272])[0]
            self.router = ipaddress.IPv4Address(data[251:255])
            self.subnetMask = ipaddress.IPv4Address(data[245:249])

    def printOffer(self):
        key = ['DHCP Server', 'Offered Ip address', 'subnet mask', 'lease time (secs)' , 'Relay agent']
        val = [self.DHCPServerIdentifier, self.offerIP, self.subnetMask, self.leaseTime, self.router]
        for i in range(5):
            print(key[i] + ': ' + str(val[i]))

class Dhcp_Request:
    def __init__(self,data):
        self.data_b = data

    def Dhcp_Request_packet(self):
        opcode = b'\x01'
        hardware_type = b'\x01'
        hardware_addr_len = b'\x06'
        hop_count = b'\x00'
        trans_id = self.data_b[4:8]
        num_of_sec = b'\x00\x00'
        broadcast_flag = b'\x00\x00'
        client_ip = b'\x00\x00\x00\x00'
        allocated_ip = b'\x00\x00\x00\x00'
        server_ip = b'\x00\x00\x00\x00'
        relay_agent_ip = b'\x00\x00\x00\x00'
        client_mac_addr = self.data_b[28:34]
        client_addr_padding = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        server_host_name = b'\x00' * 67
        boot_file_name = b'\x00' * 125
        opt_dhcp_magic_cookie = b'\x63\x82\x53\x63'
        opt_dhcp_type = b'\x35\x01\x03'
        opt_mac = b'\x3d\x07\x01' + self.data_b[28:34]
        opt_reqested_ip = b'\x32\x04' + self.data_b[16:20]
        opt_server_ip = b'\x36\x04'+ self.data_b[245:249]
        opt_param =  b'\x37\x03\x03\x01\x06'
        opt_end = b'\xff'
        packet = opcode + hardware_type + hardware_addr_len + hop_count + trans_id + num_of_sec + broadcast_flag
        packet += client_ip + allocated_ip + server_ip + relay_agent_ip
        packet += client_mac_addr + client_addr_padding + server_host_name + boot_file_name
        packet += opt_dhcp_magic_cookie + opt_dhcp_type + opt_mac + opt_reqested_ip + opt_server_ip + opt_param + opt_end
        return packet;


class Dhcp_Ack:
    def __init__(self, data, transID):
        self.data = data
        self.transID = transID
        self.offerIP = ''
        self.DHCPServerIdentifier = ''
        self.leaseTime = ''
        self.subnetMask = ''
        self.unpack(self.data)
        self.save_Ack()
        
    def unpack(self,data):
        if data[4:8] == self.transID :
            self.offerIP = ipaddress.IPv4Address(data[16:20])
            self.DHCPServerIdentifier = ipaddress.IPv4Address(data[273:277])
            self.leaseTime = struct.unpack('!L',data[268:272])[0]
            self.subnetMask = ipaddress.IPv4Address(data[245:249])

    def save_Ack(self):
        ip_dict = {"Dhcp Server": str(self.DHCPServerIdentifier),
        "Ip address": str(self.offerIP),
        "subnet mask": str(self.subnetMask),
        "Lease": str(self.leaseTime)
        }
        try:
            with open('ack.txt', 'w+') as json_file:
              json.dump(ip_dict, json_file)
              print("Dhcp information saved succesfully\n" + "try Ip_info option to know more\n")
            print('------------------\n')
        except IOError as e:
            print("Could not save ip information:", e)

class Dhcp_Release(get_class):
    def __init__(self,ip_addr,server_addr):
        get_class.__init__(self)
        self.ip_addr = ip_addr
        self.server_addr = server_addr

    def Dhcp_Release_packet(self):
        opcode = b'\x01'
        hardware_type = b'\x01'
        hardware_addr_len = b'\x06'
        hop_count = b'\x00'
        trans_id = self.get_trans_id
        num_of_sec = b'\x00\x00'
        broadcast_flag = b'\x00\x00'
        client_ip = self.ip_addr
        allocated_ip = b'\x00\x00\x00\x00'
        server_ip = b'\x00\x00\x00\x00'
        relay_agent_ip = b'\x00\x00\x00\x00'
        client_mac_addr = self.get_mac_in_bytes
        client_addr_padding = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        server_host_name = b'\x00' * 67
        boot_file_name = b'\x00' * 125
        opt_dhcp_magic_cookie = b'\x63\x82\x53\x63'
        opt_dhcp_type = b'\x35\x01\x07'
        opt_mac = b'\x3d\x07\x01' + self.get_mac_in_bytes
        opt_server_ip = b'\x36\x04'+ self.server_addr
        opt_end = b'\xff'
        packet = opcode + hardware_type + hardware_addr_len + hop_count + trans_id + num_of_sec + broadcast_flag
        packet += client_ip + allocated_ip + server_ip + relay_agent_ip
        packet += client_mac_addr + client_addr_padding + server_host_name + boot_file_name
        packet += opt_dhcp_magic_cookie + opt_dhcp_type + opt_mac + opt_server_ip + opt_end
        return packet;
