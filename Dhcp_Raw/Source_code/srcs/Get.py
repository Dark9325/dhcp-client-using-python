import struct
from random import randint
from scapy.all import get_if_raw_hwaddr, conf

class get_class:
    def __init__(self):
        self.get_mac_in_bytes = get_mac_in_bytes(self)
        self.get_trans_id = get_trans_id()

def get_mac_in_bytes(self):
    macb = get_if_raw_hwaddr(conf.iface)[1]
    return macb

def get_trans_id():
    trans_id = b''
    for i in range(4):
            rand_id = randint(0,255)
            trans_id += struct.pack('!B', rand_id)
    return trans_id
