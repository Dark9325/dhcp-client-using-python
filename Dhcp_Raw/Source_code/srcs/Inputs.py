from Sockets import socket_com, socket_read, socket_release
import sys

def opt_validation(opt_val):
        if opt_val <= 0 or opt_val > 4:
            print("Invalid option")
            sys.exit()
        else :
            if opt_val == 1:
                socket_com()
            elif opt_val == 2:
                socket_read()
            elif opt_val == 3:
                socket_release()
            else:
                sys.exit()


def input_options() :
    print("*** Running Dhcp_client (Linux_version)***\n")
    print("Warning: Log in as a Root user\n" + "Warning: Make sure Port 68 is not in use\n" + "Warning : Make sure Media Devices such as Wifi/lan are connected\n")
    print("Warning: Free port with cmd (if port in use): sudo fuser -k 68/udp\n")
    print("Select an option to continue\n")

    user_input = raw_input("1.Renew IP\n" + "2.IP Info\n" + "3.Release IP\n" + "4.Exit\n")
    if user_input.isdigit():
        opt = int(user_input)
        opt_validation(opt)
    else:
        print("Invalid Option\n")
        sys.exit()
